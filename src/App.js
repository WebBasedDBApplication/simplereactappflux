import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
            <p>
                You clicked {this.props.sampleStore.clickCounter}
            </p>
            <button onClick={() => this.props.incrementCounter()}>Click me</button>
        </header>
      </div>
    );
  }
}

export default App;
