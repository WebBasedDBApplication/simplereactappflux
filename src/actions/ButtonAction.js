import SampleDispatcher from '../dispatcher/SampleDispatcher';

export const CLICK_ACTION = "CLICK_ACTION";

export const Actions = {

    click(){
        SampleDispatcher.dispatch({
            type: CLICK_ACTION
        });
    }
}
