import React, { Component } from 'react';
import { Container } from 'flux/utils';
import SampleStore from '../store/SampleStore';
import { Actions } from '../actions/ButtonAction';
import App from '../App';

class SampleContainer extends Component {

    static getStores(){
        return [
            SampleStore
        ];
    }

    static calculateState() {
        return {
            sampleStore: SampleStore.getState(),

            incrementCounter: Actions.click
        }
    }

    render(){
        return (
            <App {...this.state} />
        );
    }

}

export default Container.create(SampleContainer);

