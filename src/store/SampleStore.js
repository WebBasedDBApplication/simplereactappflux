import { ReduceStore } from 'flux/utils';
import SampleDispatcher from '../dispatcher/SampleDispatcher';
import { CLICK_ACTION } from '../actions/ButtonAction';

class SampleStore extends ReduceStore {

    constructor(){
        super(SampleDispatcher)
    }

    getInitialState(){
        return {
            clickCounter: 0
        };
    }

    reduce(state, action){
        switch (action.type){
            case CLICK_ACTION:
                state.clickCounter = state.clickCounter + 1;
                return {...state};
            default:
                return state;
        }
    }
}

export default new SampleStore();
